= Development Notes
:nofooter:

== Steam Defined Environment Variables

When launching a game, Steam defines a series of useful environment variables.

[cols="1s,6,2m"]
|===
| Name | Description | Example

| GAMESCOPE_WAYLAND_DISPLAY
|
Name of the wayland display assigned to the current gamescope session.

Useful to detect if we are currently running in game mode.
| gamescope-0

| STEAM_CLIENT_CONFIG_FILE
| Full path to the Steam config file.
| /home/deck/.local/share/Steam/steam.cfg

| STEAM_COMPAT_APP_ID
| AppId used for the `compatdata` folder. For Non-Steam Apps, this may differ from `SteamAppId`.
| 646570

| STEAM_COMPAT_CLIENT_INSTALL_PATH
| Path to the Steam installation
| /home/deck/.local/share/Steam

| STEAM_COMPAT_DATA_PATH
| Path where the Proton/Wine prefix is located at.
| /home/deck/.local/share/Steam/steamapps/compatdata/646570

| STEAM_COMPAT_INSTALL_PATH
| Install path of the Steam Game.
| /home/deck/.local/share/Steam/steamapps/common/SlayTheSpire

| STEAM_COMPAT_SHADER_PATH
| Path of the Shader Cache for this game.
| /home/deck/.local/share/Steam/steamapps/shadercache/646570

| SteamAppId
| Steam Store App Id for the currently executed game. Will be `0` for Non-Steam Games.
| 646570

| SteamAppUser
| Username of the owner of the app/game currently being run. May differ from `SteamUser` if running via family sharing.
| exampleuser

| SteamDeck
| Set to `1` when running on a Steam Deck.
| 1

| SteamOS
| Set to `1` when running on SteamOS.
| 1

| SteamUser
| Username of the user currently logged into Steam.
| exampleuser
|===
